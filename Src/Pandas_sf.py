from scipy import integrate
import numpy as np
import pandas as pd
import collections

def Arrays():
    df1= np.loadtxt('1d_info.dat')
    print("First data frame for difractograms =",df1)
    h = [item[0] for item in df1]
    print("\nh", h)
    k = [item[1] for item in df1]
    print("\nk",k)
    l = [item[2] for item in df1]
    print("\nl",l)
    fideg = [item[3] for item in df1]
    print("\nAngle",fideg)
    intensity = [item[4] for item in df1]
    print("\nIntensity",intensity)
    hkl = [list(x) for x in zip(h , k , l)]
    print("\nhkl",hkl)
    cgdir_df = np.loadtxt('result.dat', dtype=(float))
    print("length of cg_dir",len(cgdir_df))
    if cgdir_df.ndim == 1: cgdir_df = cgdir_df.reshape(1,3)
    cg_dir = cgdir_df
    print("\ncgdir",cg_dir)
    with open('caso.dat','r') as file:
        direction_texture = file.read()
    print("\ntexture direction", direction_texture)
    print("\nlendirecton=",len(direction_texture))
    if len(direction_texture)>= 8:
        true_dir = (direction_texture [3] + direction_texture [5] + direction_texture [7])
    else:
        true_dir = (direction_texture [2] + direction_texture [4] + direction_texture [6])
    texture_direction = [int(s) for s in true_dir]
    print("\ntrue direction=",true_dir)
    print("\nfinal direction", texture_direction)
    return (intensity,fideg,hkl,cg_dir,texture_direction)

if __name__ == "__main__":
    intensity,fideg,hkl,cg_dir,texture_direction = Arrays()



