@echo off
echo Installing dependencies needed for Grazing module
echo
echo This Source Code Form is subject to the terms of the Mozilla Public
echo License, v. 2.0. If a copy of the MPL was not distributed with this
echo file, You can obtain one at http://mozilla.org/MPL/2.0/.
echo

echo PATH

set ACTIVATEPATH=%HOMEDRIVE%%HOMEPATH%\miniconda3\Scripts\activate.bat

call %ACTIVATEPATH%

echo direction is %conda_dir%
echo Installing packages
echo Installing git 

 call conda update --all

 call conda install -c anaconda git

echo getting repository

 call git clone https://gitlab.com/cimav-tools/grazing.git

echo InstallingPyside2

 pip install PySide2

echo Installing matplotlib

 pip install matplotlib

echo Installing scipy

 pip install scipy

echo Installing pandas

 pip install pandas

echo Installing gfortran setup

call conda install -c msys2 m2w64-gcc-fortran



pause