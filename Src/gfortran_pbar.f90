!!---- This file is part of the "Esmeralda" project,
!!---- a tool for the treatment of Laue diffraction data
!!----
!!---- The "Esmeralda" project is distributed under LGPL. In agreement with the
!!---- Intergovernmental Convention of the ILL, this software cannot be used
!!---- in military applications.
!!----
!!---- Copyright (C)  2010-2012  Institut Laue-Langevin (ILL), Grenoble, FRANCE
!!----
!!---- Authors: Luis Fuentes-Montero    (ILL)
!!----          Juan Rodriguez-Carvajal (ILL)
!!----
!!---- This library is free software; you can redistribute it and/or
!!---- modify it under the terms of the GNU Lesser General Public
!!---- License as published by the Free Software Foundation; either
!!---- version 3.0 of the License, or (at your option) any later version.
!!----
!!---- This library is distributed in the hope that it will be useful,
!!---- but WITHOUT ANY WARRANTY; without even the implied warranty of
!!---- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!!---- Lesser General Public License for more details.
!!----
!!---- You should have received a copy of the GNU Lesser General Public
!!---- License along with this library; if not, see <http://www.gnu.org/licenses/>.
!!----
 Module compilers_specific
 !  use ifport                                                              ! ifort
   implicit none
   private
   public :: prog_bar
   integer, save  :: pbarp_old = -1

  contains
    Subroutine Prog_Bar(p_min,pos_i,p_max,txt)
      integer           , intent(in)    :: p_min, pos_i, p_max
      character(len=*)  , intent(in)    :: txt
      integer                 :: pbarp, p_siz, local_min, local_max, i
      character(len=60)       :: bar
      if( p_max < p_min )then
        local_max = p_min
        local_min = p_max
      else
        local_min = p_min
        local_max = p_max
      end if

      p_siz=local_max-local_min
      pbarp = int( real(pos_i - local_min) / real(p_siz) * 60 )
      if (pbarp/=pbarp_old) then
 !       open (unit=6, carriagecontrol='fortran')                                ! ifort
        do i=1,60,1
          if(i <= pbarp)then
              bar(i:i)='/'
          else
              bar(i:i)='-'
          end if
        end do
        write(unit=*,advance='no',fmt='(A)') char(13)              ! gfortran & g95
        write(unit=*,advance='no',fmt='(A)') '['//bar//']'//txt    ! gfortran & g95
        call flush()                                               ! gfortran & g95
 !       write(unit=6,fmt='(a1,a1,a)') '+',char(13),'['//bar//']'//txt   ! ifort
      end if
      pbarp_old=pbarp
      return
    End Subroutine Prog_Bar
 End module compilers_specific
