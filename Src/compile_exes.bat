echo setting up exes needed
echo dependencies installed - Run startup batch
echo setting up Crysfml

set ACTIVATEPATH=%HOMEDRIVE%%HOMEPATH%\anaconda3\Scripts\activate.bat

call %ACTIVATEPATH%

 gfortran -o pg_directions1.exe pg_directions1.f90

set CRYSFML=crysfml_snapshot_n02_nov_2017

echo "anaelu_C_L_I  Program Compilation -static"

gfortran -c -O2 -ffree-line-length-none -funroll-loops       gfortran_pbar.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops       input_args.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops       rw_n_use_edf.f90

gfortran -c -O2 -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC  global_types_n_deps.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC            mic_tools.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC              calc_2d.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC      sample_CLI_data.f90

gfortran -c -O2 -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC         anaelu_C_L_I.f90

echo " Linking"

gfortran -o anaelu_1d_powdr.exe *.o  -L%CRYSFML%\GFortran\LibC -static -lcrysfml

del *.o *.mod

pause