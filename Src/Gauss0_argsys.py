import numpy as np ; import math
import scipy.integrate as sq
from scipy import special
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
from matplotlib import cm, colors
from numpy import sin,cos,pi,abs, arccos, exp, tan
from mpl_toolkits.mplot3d import Axes3D
from Triclinic_poles_def import Triclinic
from Pandas_sf import Arrays
import sys



print("\nEntering Gauss0_argsys")
#ARGSYS number
#Arguments from the GUI are sent to the main program Gauss0_argsys.py
# n is reffered to the number of argument using sys.argv method
n = len(sys.argv)
print("\nTotal arguments passed:", n)
#Printing the program in which the arguments are sent
print("\nName of Python script:", sys.argv[0])
#List of argument will result alway in 9. This list will be sent to be update with the
#pandas routine PandasSF.py
a,b,c,alpha,gamma,beta,fwhm2,FWHMdeg,crystal,lambdad = sys.argv[1:]
print("\nInside Gauss0_argsys, characteristic parameters:",a,b,c,alpha,gamma,beta,fwhm2,FWHMdeg,crystal,lambdad)

#update-parameters sent to the GUI with the program Pandas_SF
list1 = []
for i in [a,b,c,alpha,gamma,beta,fwhm2,FWHMdeg,crystal,lambdad]:
    list1.append(i)
list1 = [float(s) for s in list1]
a,b,c,alpha,gamma,beta,fwhm2,FWHMdeg,crystal,lambdad = list1
print(list1)
print(a,b,c,alpha,gamma,beta,fwhm2,FWHMdeg,crystal,lambdad)

#Calling parameter from the triclinic routines -
#Gui calls CrysFML, Eduardo program and the triclinic routine in this respective order
a,b,c,alpha,beta,gamma,cg_dir,texture_direction,thetadeg,phideg,taudeg,gammadeg,theta_list,\
thetadeg_list, phi_list,phideg_list = Triclinic(a,b,c,alpha,beta,gamma)

print("\nthetadeg, phideg, taudeg, gammadeg in Gauss__argsys after Triclinic:\n",thetadeg, phideg,taudeg, gammadeg, "\n")

#Calling list from pandas_sf.py routine
intensity,fideg,hkl,cg_dir,texture_direction = Arrays()

print("\nBack from pandas_sf:\n", intensity,fideg,hkl,cg_dir,texture_direction)

'''Defining Gaussian distribution parameters:'''
grad_rad = pi / 180.0 ; rtg = 180.0 / pi
FWHM = FWHMdeg * grad_rad
print (" IPF FWHM (degrees and radians) = ", FWHMdeg, "{:.4f}".format(FWHM))

'''functions'''
#Orthodromic distance, function for inverse pole figure
def R1(eta, chi):
    RR = 0.0
    for test in range(0,len(theta_list)):
        theta = theta_list[test]
        phi = phi_list[test]
        if theta == 0.0: 
            rho = eta 
        else:
            d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
            if d >= 1.0: d = 1.0
            elif d <= -1.0 : d = -1.0
            rho = arccos(d)
        RR = RR + exp(-2.7725887*(rho/FWHM)**2)
    return RR
def Rs(eta, chi):
    return R1(eta,chi)*sin(eta)
def Rnorma(eta, chi):
    return (4.0*pi/R0)*R1(eta, chi)
def Rns(eta, chi):
    return Rnorma(eta, chi) * sin(eta)
def Rvar(psi):
    if tau == 0.0 or tau == math.pi:
        RRR = Rnorma(fi,psi) 
    else:
        A = cos(tau)*cos(fi) + sin(tau)*sin(fi)*cos(psi)
        eta = arccos(A)
        cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
        B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
        if np.all(B >= 1.0): B = 1.0
        elif np.all(B <= -1.0) : B = -1.0
        if psi <= pi:
            chi = gamma - arccos(B)
        else:
            chi = gamma + arccos(B)
        RRR = Rnorma(eta,chi)
    return RRR
def pfg(fi):
    pfgr = sq.quad(Rvar, 0, 2.0*pi)
    return pfgr[0]


# Calculating R surface integral:
R1suma = sq.dblquad(Rs, 0, 2.0*pi, 0, pi)
R0 = R1suma[0]
print (" Integral of initial IPF, R0 = ", R0)

# Verifying normalization, getting max value of the IPF
Rnormsum = sq.dblquad(Rns, 0, 2.0*pi, 0, pi)
Rnorsu = Rnormsum[0]
print (" Integral of the normalized IPF = ", Rnorsu)
print (" 4*pi = ", 4.0*pi)
print (" R_normalized(max) =", Rnorma(theta_list[0], phi_list[0]))
fideg2 = [x/2.0 for x in fideg]
pf_dir = []; RBB_dir = []
print("\n Texture factors for Grazing and Bragg-Brentano")
print(" PF(theta_Bragg)    R(h, k, l)")
for Npeak in range(0, len(taudeg)):
    tau = taudeg[Npeak]*grad_rad
    gama = gammadeg[Npeak]*grad_rad
    fidg = np.zeros(180); pfg = np.zeros(180)
    fi = fideg2[Npeak]*grad_rad
    pfval = sq.quad(Rvar, 0, 2.0*pi)
    pfvalue = pfval[0]/(2.0*pi)
    pf_dir.append(pfvalue)
    RBB = Rnorma(tau,gama)
    RBB_dir.append(RBB)
    print(pfvalue, RBB)

print("\n",hkl)
print("\n",fideg)

#Pairing of array for difractograms
#pf_dir = [0 if math.isnan(x) else x for x in pf_dir]
Bragg_int = [x*y for x,y in zip(RBB_dir,intensity)]
print ("intensity bragg brentano = ", Bragg_int, "\n")
Bragg_dif = [list(x) for x in zip(Bragg_int, fideg)]
print ("nested list bragg brentano = ", Bragg_dif, "\n")

text_int = [x*y for x,y in zip(pf_dir,intensity)]
print ("new integral intensity = ", text_int, "\n")
Difract_nl = [list(x) for x in zip(text_int , fideg)]
print("new nested list for difractogram" , Difract_nl , "\n")
print("hkl nest list", hkl,"\n")

crysfml_int = [list(x) for x in zip(intensity,fideg)]

'''Inverse Pole Figure'''
#Calculating inve+rse pole figure on the reference sphere
Q = 0.7 #max_ipf  Radius of the reference sphere
eta, chi = np.mgrid[0:pi:51j, 0:2*pi:101j]
Rsum = 0.0
Rsum2 = 0.0
for Nchip in range(0,len(theta_list)):
    theta = theta_list[Nchip]
    phi = phi_list[Nchip]
    p = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    p1 = arccos(p)
    R = exp(-2.7725887*(p1/FWHM)**2)
    R1 = R + Q
    Rsum = Rsum + R1
    Rsum2 = Rsum2 + R
    print("Rsum=", Rsum)
    print("Rsum2", Rsum2)
    print("maxvalrsum=",np.max(Rsum))
    print("maxvalrsum2=",np.max(Rsum2))
    #Rsum = Rsum + Q
    x = Rsum * sin(eta) * cos(chi)
    y = Rsum * sin(eta) * sin(chi)
    z = Rsum * cos(eta)

# PFtau=np.zeros(3); PFgamma=np.zeros(3)
# for Npeak in range (0, 3):
#     PFtau[Npeak] = taudeg[Npeak] * pi / 180.0
#     PFgamma[Npeak] = gammadeg[Npeak] * pi / 180.0
#     nombre = "PoleFig" + str(Npeak) + ".dat"
#     print(nombre)

# for Npeak in range(0, 3):
#     file = open("polefig" + str(Npeak) + ".dat", "w")
#     tau = PFtau[Npeak]; gamma = PFgamma[Npeak]
#     fidg = np.zeros(180); pfg = np.zeros(180)
#     fidegree = 0.0
#     for fidegree in range(0, 181, 1):
#         n = int(fidegree/5)
#         fidg[n] = fidegree
#         fi = fidegree/rtg
#         pf = sq.quad(Rvar, 0, 2.0*pi)
#         pfg[n] = pf[0]/(2.0*pi)
#         file.write(str(fidg[n])+" "+ str(pfg[n])+"\n")
#     file.close()

# for Npeak in range(0,3):
#     f_name = "polefig"+str(Npeak)+".dat"
#     data_g = np.loadtxt(f_name)
#     print("\nfile name =", data_g)
#     X = data_g[:,0]
#     Y = data_g[:,1]
#     plt.plot(X,Y)
#     plt.title('Direct pole figure' + ' ' +str(hkl[Npeak]))
#     plt.savefig("Polefigure"+str(Npeak)+".png")
#     plt.close()

#FWHM Gaussian Equation - Graphicating the 1D XRDS
def gaussian_dist(x, a , mean, sd):
    return a/(sd*np.sqrt(2.0*np.pi))*np.exp(-(x - mean)**2.0/(2.0*sd**2.0))

if __name__ == "__main__":

    array_txt = np.loadtxt("grazing.txt",usecols=(0, 1), skiprows=0)

    x_exp_grazing = array_txt[:,0]
    print("degrees=",x_exp_grazing)

    y_exp_grazing = array_txt[:,1]
    print("intensity=",y_exp_grazing)

    lst = Difract_nl

    xgrazing = np.linspace(0,80,10000)
    ygrazing = np.zeros((10000))

    ######
    std = fwhm2/2.35
    inst = 0.2
    k = 0.9
    pwidth = [(k*lambdad)/ (crystal*cos(i*pi/180.0)) for i in fideg]
    pwdith2 = [x+inst for x in pwidth]
    
    print("fideg",fideg)
    #pwidth = [x*180.0/pi for x in pwidth]
    #pwdith2 = [x*180.0/pi for x in pwdith2]
    print("pwidth",pwidth)
    print("\npwidth2",pwdith2)

    ######
    for i,xrdgeom in enumerate(xgrazing):
        for j,k in zip(lst,pwdith2):
            ygrazing[i] += gaussian_dist(xrdgeom , j[0] , j[1] , k)

    print("ygrazing",ygrazing)
    plt.figure(2)
    plt.plot(xgrazing, ygrazing, '-k', label='1,1,1')
    plt.xlabel('2θ(degrees)')
    plt.ylabel('Intensity(a.u)')
    plt.title('Grazing incidence XRD')
    plt.savefig('grazing_fig0.png')

    factor = max(y_exp_grazing)/max(ygrazing)
    ygrazing = ygrazing*factor

    plt.figure(3)
    plt.plot(xgrazing, ygrazing, '-k', label='1,1,1')
    plt.plot(x_exp_grazing,y_exp_grazing,color='red')
    plt.xlabel('2θ(degrees)')
    plt.ylabel('Intensity(a.u)')
    plt.title('Grazing incidence XRD')
    plt.savefig('grazing_fig1.png')

    array_txt = np.loadtxt("bragg.txt",usecols=(0, 1), skiprows=0)

    x_exp_bragg = array_txt[:,0]
    print("degrees=",x_exp_bragg)

    y_exp_bragg = array_txt[:,1]
    print("intensity=",y_exp_bragg)

    lst1 = Bragg_dif

    xbragg = np.linspace(0,80,10000)
    ybragg = np.zeros((10000))

    for i,xrdgeom in enumerate(xbragg):
        for j,k in zip(lst1,pwdith2):
            ybragg[i] += gaussian_dist(xrdgeom , j[0] , j[1] , k)

    plt.figure(4)
    plt.plot(xbragg, ybragg, '-k', label='0,0,1')
    plt.xlabel('2θ(degrees)')
    plt.ylabel('Intensity(a.u)')
    plt.title('Bragg Brentano XRD')
    plt.savefig('bragg_fig0.png')

    factor2 = max(y_exp_bragg)/max(ybragg)
    ybragg = ybragg*factor2

    plt.figure(5)
    plt.plot(xbragg, ybragg, '-k', label='0,0,1')
    plt.plot(x_exp_bragg,y_exp_bragg,color='red')
    plt.xlabel('2θ(degrees)')
    plt.ylabel('Intensity(a.u)')
    plt.title('Bragg Brentano XRD')
    plt.savefig('bragg_fig1.png')

    #pl.show()

    lst2 = crysfml_int

    crysfml_x = np.linspace(0,80,10000)
    crysfml_y  = np.zeros((10000))

    for i,xrdgeom in enumerate(crysfml_x):
        for j,k in zip(lst2,pwdith2):
            crysfml_y[i] += gaussian_dist(xrdgeom , j[0] , j[1] , k)

    plt.figure(6)
    plt.plot(crysfml_x, crysfml_y, '-k', label='0,0,1')
    plt.xlabel('2θ(degrees)')
    plt.ylabel('Intensity(a.u)')
    plt.title('Crysfml XRD')
    plt.savefig('Crysfml_XRD.png')

### Drawing IPF
colorfunction= Rsum
norm = colors.Normalize()
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x, y, z)
surface = ax.plot_surface(x, y, z,label='Inverse Pole' ,  rstride=1, cstride=1 , facecolors=cm.jet(norm(colorfunction)))
val = 1.2198085261644226024779405283314
ax.set_xlim(-val*np.max(Rsum), val*np.max(Rsum)); ax.set_ylim(-val*np.max(Rsum),val*np.max(Rsum))
title = "Inverse pole figure" + ' ' +str(texture_direction)
cbar=plt.colorbar(surface)
ax.set_title(title)
plt.savefig("inverse pole figure.png")

print("-Model finished-")
