import numpy as np ; import math
from numpy import sin,cos,pi,abs, arccos, exp, tan
from Pandas_sf import Arrays
import os

intensity,fideg,hkl,cg_dir,texture_direction = Arrays()

print(intensity,fideg,hkl,cg_dir,texture_direction)

'''Values not affected, lattice parameters'''
np.set_printoptions(suppress=True,formatter={'float_kind':'{:0.2f}'.format})
rtg = 180.0/math.pi

a = 7.688 ; alpha = 90/rtg
b = 7.539 ; beta  = 136.06/rtg
c = 10.515 ; gamma = 90/rtg

l33a = 1.0 + 2.0*cos(alpha)*cos(beta)*cos(gamma)
l33b = cos(alpha)**2 + cos(beta)**2 + cos(gamma)**2
l33c = l33a - l33b
L = np.array([[a,b*cos(gamma),c*cos(beta)], \
[0,b*sin(gamma),c*(cos(alpha)-(cos(beta)*cos(gamma)))/sin(gamma)], \
[0,0, (c*np.sqrt(l33c))/sin(gamma)]])

print("\nMatrix L:\n", np.array(L))
print("\nInverse Matrix:\n",  np.linalg.inv(L))

'Representa el vector i,j,k dependiendo del numero donde se asigne, Cartesian Coordinates'
Vt = np.array([1,0,0]) 
print("\n Base \n:",Vt)
ac = L.dot(Vt)
print("\nCubic Base a\n:",ac)

Vt = np.array([0,1,0]) 
print("\n Base \n:",Vt)
bc = L.dot(Vt)
print("\nCubic Base b\n:",bc)

Vt = np.array([0,0,1]) 
print("\n Base \n:",Vt)
cc = L.dot(Vt)
print("\nCubic Base c\n:",cc)

L = np.linalg.inv(L)
print("\nL inverse:", L)
Vc = np.array([1,1,1])
print("\n Base \n:",Vc)
Vt = L.dot(Vc)
print("\nTriclinic Base\n:",Vt)

'Getting matrix L into vectors'
a1 = np.array([[a],[0],[0]])
a2 = np.array([[b*cos(gamma)],[b*sin(gamma)],[0]])
a3 = np.array([[c*cos(beta)],[c*(cos(alpha)-(cos(beta)*cos(gamma)))/sin(gamma)],[c*np.sqrt(l33c)/sin(gamma)]])

print("\nAll vectors=", a1 ,a2,a3)


cross_prod = np.cross(a2,a3, axisa=0, axis= 0)
print("cross_prod=",cross_prod,"\n")
volume = a1.T.dot(cross_prod)
print("\nvolume = ", volume)

'Getting a* b* and c*'
b1 = (1/volume)*(np.cross(a2,a3, axisa=0, axis= 0))
b2 = (1/volume)*(np.cross(a3,a1, axisa=0, axis= 0))
b3 = (1/volume)*(np.cross(a1,a2, axisa=0, axis= 0))

print("b1=",b1,"\n")
print("b2=",b2,"\n")
print("b3=",b3,"\n")

Q = np.array([b1,b2,b3]).T
print("\nQ=",Q)

###############################
' HkL for the IPF direction'
###############################
hkl_dir = texture_direction

print("\nhkl",hkl_dir)
Vc = Q.dot(hkl_dir)
print("\nVc",Vc)

print("\nshapeVc",np.shape(Vc))
modVc = np.sqrt(Vc[0,0]**2 + Vc[0,1]**2 + Vc[0,2]**2)
print("\nmodVc = ",modVc)  
polar = math.acos(Vc[0,2]/modVc)
print("\npolar = ",rtg * polar)
thetadeg = polar
azim = math.acos((Vc[0,0]/modVc)/sin(polar))
if math.isnan(azim): azim = 0
print("\nazim = ",rtg * azim)
phideg = azim


######################################################################################
'''Tau and phi for a list of hkl (crystalographic directions-Eduardo)'''
######################################################################################
list_hkl = cg_dir
print("\nlisthkl =", list_hkl)

Vc_list=[]
for i in list_hkl:
    Vc_list.append(Q.dot(i))
print("\nVc_list=",Vc_list, np.shape(Vc_list))
Vc_list = [l.tolist() for l in Vc_list]
Vc_list = np.array(Vc_list)
print("\nVc_tolist=",Vc_list,np.shape(Vc_list))
Vc_list1= Vc_list.flatten().reshape(Vc_list.shape[0],Vc_list.shape[1]*Vc_list.shape[2])
print("\nC=",Vc_list, np.shape(Vc_list))

modVc_list = []
for i in Vc_list1:
    modVc_list.append(np.sqrt(i[0]**2 + i[1]**2 + i[2]**2))
print("\nmodVc = ",modVc_list , np.shape(modVc_list))
modVc_list = [float(i) for i in modVc_list]

Vc_polar = []
for i in Vc_list1:  
    Vc_polar.append(i[2])
print("\nVc_polar 0,2=",Vc_polar,np.shape(Vc_polar))
Vc_polar = [float(i) for i in Vc_polar]

term_polar = []

# get last index for the lists for iteration
end_index = len(Vc_polar)

for i in range(end_index):
    term_polar.append(Vc_polar[i]/modVc_list[i])
theta_list = term_polar

#term_polar = Vc_polar / modVc_list
print("\nterm_polar",term_polar)
polar = [rtg*math.acos(i) for i in term_polar]
print("\npolar=",polar) 
thetadeg_list = polar

Vc_azim = []
for i in Vc_list1:  
    Vc_azim.append(i[0])
print("\nVc_azim 0,0=",Vc_azim,np.shape(Vc_azim))
Vc_azim = [float(i) for i in Vc_azim]

term_azim = []
# get last index for the lists for iteration
end_index = len(Vc_azim)
for i in range(end_index):
    term_azim.append(Vc_azim[i]/modVc_list[i])
print("\nterm_azim",term_azim)
phi_list = term_azim  
sin_polar = [sin(i) for i in polar]


azim_angle = []
end_index = len(term_azim)
for i in range(end_index):
    azim_angle.append(rtg*(term_azim[i]/sin_polar[i]))
azim_angle = [0 if math.isnan(i) else i for i in azim_angle]  
print("azim_angle=",azim_angle)
phideg_list = azim_angle

###############################################
'''Tau and gamma for hkl reflections '''
###############################################

list_hkl = hkl
print("\nlisthkl =", list_hkl)

Vc_list=[]
for i in list_hkl:
    Vc_list.append(Q.dot(i))
print("\nVc_list=",Vc_list, np.shape(Vc_list))
Vc_list = [l.tolist() for l in Vc_list]
Vc_list = np.array(Vc_list)
print("\nVc_tolist=",Vc_list,np.shape(Vc_list))
Vc_list1= Vc_list.flatten().reshape(Vc_list.shape[0],Vc_list.shape[1]*Vc_list.shape[2])
print("\nC=",Vc_list, np.shape(Vc_list))

modVc_list = []
for i in Vc_list1:
    modVc_list.append(np.sqrt(i[0]**2 + i[1]**2 + i[2]**2))
print("\nmodVc = ",modVc_list , np.shape(modVc_list))
modVc_list = [float(i) for i in modVc_list]

Vc_polar = []
for i in Vc_list1:  
    Vc_polar.append(i[2])
print("\nVc_polar 0,2=",Vc_polar,np.shape(Vc_polar))
Vc_polar = [float(i) for i in Vc_polar]

term_polar = []

# get last index for the lists for iteration
end_index = len(Vc_polar)

for i in range(end_index):
    term_polar.append(Vc_polar[i]/modVc_list[i])


#term_polar = Vc_polar / modVc_list
print("\nterm_polar",term_polar)
polar2 = [rtg*math.acos(i) for i in term_polar]
print("\npolar=",polar2) 
taudeg = polar2

Vc_azim = []
for i in Vc_list1:  
    Vc_azim.append(i[0])
print("\nVc_azim 0,0=",Vc_azim,np.shape(Vc_azim))
Vc_azim = [float(i) for i in Vc_azim]

term_azim = []
# get last index for the lists for iteration
end_index = len(Vc_azim)
for i in range(end_index):
    term_azim.append(Vc_azim[i]/modVc_list[i])
print("\nterm_azim",term_azim)
sin_polar = [sin(i) for i in polar2]


azim_angle2 = []
end_index = len(term_azim)
for i in range(end_index):
    azim_angle2.append(rtg*(term_azim[i]/sin_polar[i]))
azim_angle2 = [0 if math.isnan(i) else i for i in azim_angle2]  
print("azim_angle2=",azim_angle2)
gammadeg = azim_angle2