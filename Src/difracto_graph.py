import os
from matplotlib import pyplot as plt
import numpy as np

def read_difractogram_file_bb(my_file_path = self.brag):

    array_txt = np.loadtxt(my_file_path,usecols=(0, 1), skiprows=1)

    x = array_txt[:,0]
    print("degrees=",x)

    y = array_txt[:,1]
    print("intensity=",y)

    data = np.column_stack([x, y])
    np.savetxt('bragg.txt', data, fmt=['%d','%d'])

    return x,y


def read_difractogram_file_gi(my_file_path = "data_bb.xy"):

    array_txt = np.loadtxt(my_file_path,usecols=(0, 1), skiprows=1)

    x = array_txt[:,0]
    print("degrees=",x)

    y = array_txt[:,1]
    print("intensity=",y)

    data = np.column_stack([x, y])
    np.savetxt('grazing.txt', data, fmt=['%d','%d'])

    return x,y


