import sys
import os
import time
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *
from matplotlib.backends.qt_compat import QtCore, QtWidgets
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from mpl_toolkits.mplot3d import Axes3D
from readcfl import read_cfl_file, CflData
from grid_atoms_xyz import read_cfl_atoms, AtomData
from readdat import read_dat_file, InstrumentData
import numpy as np
import matplotlib.pyplot as plt
import subprocess

class MultipleImgSene(QGraphicsScene):
    m_scrolling = Signal(int)
    def __init__(self, parent = None):
        super(MultipleImgSene, self).__init__(parent)

    def wheelEvent(self, event):
        #print("event.delta", event.delta())
        int_delta = int(event.delta())
        if int_delta > 0:
            self.m_scrolling.emit(int_delta)

        else:
            self.m_scrolling.emit(int_delta)

        event.accept()

class MyPopupDialog(QObject):
    def __init__(self, parent=None):
        super(MyPopupDialog, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("Widget.ui")
        self.window.setWindowTitle("Grazing XRD viewer")
        appIcon = QIcon("grazing_icon.png")
        self.window.setWindowIcon(appIcon)
        

        self.bragg = MultipleImgSene()
        self.grazing = MultipleImgSene()
        self.crysfml = MultipleImgSene()

        self.window.graphicsView_7.setScene(self.bragg)
        self.window.graphicsView_8.setScene(self.grazing)
        self.window.graphicsView_9.setScene(self.crysfml)
        
        self.value = 0
        print("value for counter=",self.value)

        fileName3 = "bragg_fig0.png"
        image3 = QImage(fileName3) 
        self.pixmap_3 = QPixmap.fromImage(image3)
        self.bragg.addPixmap(self.pixmap_3)

        fileName4 = "grazing_fig0.png"
        image4 = QImage(fileName4) 
        self.pixmap_4 = QPixmap.fromImage(image4)
        self.grazing.addPixmap(self.pixmap_4)

        fileName4 = 'Crysfml_XRD.png'
        image5 = QImage(fileName4) 
        self.pixmap_5 = QPixmap.fromImage(image5)
        self.crysfml.addPixmap(self.pixmap_5)

        self.window.turn_on_button.clicked.connect(self.turn_on_exp)
        self.window.turn_off_button.clicked.connect(self.turn_off_exp)
        self.window.show()

    def turn_on_exp(self):
        self.value += 1
        self.fname = "bragg_fig" + str(self.value) + ".png"
        print("pf name=", self.fname)
        fileName3 = self.fname
        image3 = QImage(fileName3) 
        self.pixmap_3 = QPixmap.fromImage(image3)
        self.bragg.addPixmap(self.pixmap_3)

        self.fname2 = "grazing_fig" + str(self.value) + ".png"
        print("pf name=", self.fname2)
        fileName4 = self.fname2
        image4 = QImage(fileName4) 
        self.pixmap_4 = QPixmap.fromImage(image4)
        self.grazing.addPixmap(self.pixmap_4)

    def turn_off_exp(self):
        self.value -= 1
        self.fname = "bragg_fig" + str(self.value) + ".png"
        print("pf name=", self.fname)
        fileName3 = self.fname
        image3 = QImage(fileName3) 
        self.pixmap_3 = QPixmap.fromImage(image3)
        self.bragg.addPixmap(self.pixmap_3)

        self.fname2 = "grazing_fig" + str(self.value) + ".png"
        print("pf name=", self.fname2)
        fileName4 = self.fname2
        image4 = QImage(fileName4) 
        self.pixmap_4 = QPixmap.fromImage(image4)
        self.grazing.addPixmap(self.pixmap_4)

class MyPopupDialog2(QObject):
    def __init__(self, parent=None):
        super(MyPopupDialog2, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("IPF.ui")
        self.window.setWindowTitle("Grazing PF viewer")
        appIcon = QIcon("grazing_icon.png")
        self.window.setWindowIcon(appIcon)
        

        self.pfigure = MultipleImgSene()
        self.window.graphicsView_2.setScene(self.pfigure)

        self.value = 0
        print("value for counter=",self.value)
        fileName3 = "Polefigure0.png"
        image3 = QImage(fileName3) 
        self.pixmap_3 = QPixmap.fromImage(image3)
        self.pfigure.addPixmap(self.pixmap_3)

        self.window.next_button.clicked.connect(self.next_image)
        self.window.return_button.clicked.connect(self.return_image)
        self.window.show()
        
    def next_image(self):
        self.value += 1
        self.fname = "Polefigure" + str(self.value) + ".png"
        print("pf name=", self.fname)
        fileName3 = self.fname
        image3 = QImage(fileName3) 
        self.pixmap_3 = QPixmap.fromImage(image3)
        self.pfigure.addPixmap(self.pixmap_3)

    def return_image(self):
        self.value -= 1
        self.fname = "Polefigure" + str(self.value) + ".png"
        print("pf name=", self.fname)
        fileName3 = self.fname
        image3 = QImage(fileName3) 
        self.pixmap_3 = QPixmap.fromImage(image3)
        self.pfigure.addPixmap(self.pixmap_3)

    
        


class Form(QDialog):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("grazing.ui")
        self.window.setWindowTitle("Grazing")
        appIcon = QIcon("grazing_icon.png")
        self.window.setWindowIcon(appIcon)

        #Buttons
        self.window.pushButton.clicked.connect(self.popwindow)
        self.window.load_ipf_button.clicked.connect(self.popwindow2)
        self.window.Run_Button.clicked.connect(self.running_grazing)
        

        #Table Widget
        self.window.del_row_button.clicked.connect(self.remove_r)
        self.window.add_row_button.clicked.connect(self.add_r)
        columns = ['z','Label','X pos','Y pos','Z pos', 'Occ']
        self.window.tableWidget_ini.setColumnCount(len(columns))
        self.window.tableWidget_ini.setHorizontalHeaderLabels(columns)

        #controller
        self.window.h_edit.setText("1")
        self.window.k_edit.setText("1")
        self.window.l_edit.setText("1")

        self.window.a_edit.setText("5.0")
        self.window.b_edit.setText("5.0")
        self.window.c_edit.setText("5.0")

        self.window.alpha_edit.setText("90.0")
        self.window.beta_edit.setText("90.0")
        self.window.gamma_edit.setText("90.0")

        self.window.distwidth_edit.setText("60")
        self.window.fwhm_edit.setText("0.7")
        self.window.Pointgroup_edit.setText("16")
        self.window.lambda_edit.setText("1.54")
        self.window.crystal_edit.setText("500")

        #images
        self.modl_scene = MultipleImgSene()
        self.window.graphicsView_3.setScene(self.modl_scene)
        self.window.show()

        #open read cfl Tabs
        self.window.actionOpen_CFL.triggered.connect(self.actionOpen_Cfl_connect)
        self.window.actionSave_CFL.triggered.connect(self.savefile_cfl)
        self.window.action_bragg_brentano.triggered.connect(self.action_bragg_brentano)
        self.window.action_grazing_incidence.triggered.connect(self.action_grazing_incidence)


    def actionOpen_Cfl_connect(self):
        self.read_cfl()

    #####
    def action_bragg_brentano(self):
        self.readbb()
    
    def action_grazing_incidence(self):
        self.readgi()
    #####

    def popwindow(self):
        self.dialog = MyPopupDialog(self)

    def popwindow2(self):
        self.dialog = MyPopupDialog2(self)

    def set_img(self):
        fileName1 = "inverse pole figure.png"
        image1 = QImage(fileName1) 
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.modl_scene.addPixmap(self.pixmap_1)



    def remove_r(self):
        print("Row removed")
        self.window.tableWidget_ini.removeRow(self.window.tableWidget_ini.model().rowCount()-1)

    def add_r(self):
        print("Row Added")
        self.window.tableWidget_ini.insertRow(self.window.tableWidget_ini.model().rowCount())
        print(self.window.tableWidget_ini.model().rowCount())

    def running_grazing(self):
        print("\nInitializin grazing")
        print("\nStep #1 running eduardo program")
        point_group_arg = self.window.Pointgroup_edit.text()
        h_arg = self.window.h_edit.text()
        k_arg = self.window.k_edit.text()
        l_arg = self.window.l_edit.text()
        
        print("write for dat",point_group_arg + ' ' + h_arg + ',' + k_arg + ',' + l_arg)
        fil_out = open("caso.dat", "w")
        fil_out.write(point_group_arg + ' ' + h_arg + ',' + k_arg + ',' + l_arg)
        fil_out.close()

        command_pgdirections = 'pg_directions1.exe'
        print("\ncommand pg directions",command_pgdirections)
        os.system(command_pgdirections)

        print("")
        a_arg = self.window.a_edit.text()
        b_arg = self.window.b_edit.text()
        c_arg = self.window.c_edit.text()
        alpha_arg = self.window.alpha_edit.text()
        beta_arg = self.window.beta_edit.text()
        gamma_arg = self.window.gamma_edit.text()
        fwhm_width_arg = self.window.fwhm_edit.text()
        dist_width_arg = self.window.distwidth_edit.text()
        crystal_arg = self.window.crystal_edit.text()
        lambda_arg = self.window.lambda_edit.text()

        print("step #2 Calling Crysfml")
        self.cfl_file_name = "out_Cfl.cfl"
        self.write_out_cfl()

        self.info_dat = "new_params.dat"
        self.write_new_params()
        os.system('anaelu_1d_powdr.exe new_params.dat out_Cfl.cfl')


        print("Step #3 Calling grazing")
        command = 'python Gauss0_argsys.py ' + a_arg + ' ' + b_arg + ' ' + c_arg + ' ' + alpha_arg \
                                      + ' ' + beta_arg + ' ' + gamma_arg + ' ' + fwhm_width_arg \
                                      + ' ' +dist_width_arg + ' ' + crystal_arg + ' ' + lambda_arg
        print("\ncommand = ", command)
        os.system(command)
        
        fileName1 = "inverse pole figure.png"
        image1 = QImage(fileName1) 
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.modl_scene.addPixmap(self.pixmap_1)
        

    def read_cfl(self):
        self.info1 = QFileDialog.getOpenFileName(
            self.window, 'Open CFL File', ".", "Files (*.cfl)")[0]
        print("info from clf", self.info1)

        self.data = read_cfl_file(my_file_path = self.info1)

        self.window.spgr_edit.setText(str(self.data.spgr))
        self.window.a_edit.setText(str(self.data.a))
        self.window.b_edit.setText(str(self.data.b))
        self.window.c_edit.setText(str(self.data.c))
        self.window.alpha_edit.setText(str(self.data.alpha))
        self.window.beta_edit.setText(str(self.data.beta))
        self.window.gamma_edit.setText(str(self.data.gamma))
        self.window.h_edit.setText(str(self.data.h))
        self.window.k_edit.setText(str(self.data.k))
        self.window.l_edit.setText(str(self.data.l))


        self.info2 = read_cfl_atoms(my_file_path = self.info1)
        self.window.tableWidget_ini.setRowCount(len(self.info2))
        for n,atm in enumerate(self.info2):

            self.window.tableWidget_ini.setItem(n,0,QTableWidgetItem(atm.Zn))
            self.window.tableWidget_ini.setItem(n,1,QTableWidgetItem(atm.Lbl))
            self.window.tableWidget_ini.setItem(n,2,QTableWidgetItem(atm.x))
            self.window.tableWidget_ini.setItem(n,3,QTableWidgetItem(atm.y))
            self.window.tableWidget_ini.setItem(n,4,QTableWidgetItem(atm.z))
            self.window.tableWidget_ini.setItem(n,5,QTableWidgetItem(atm.b))

    def savefile_cfl(self):
        self.cfl_file_name = QFileDialog.getSaveFileName(
            self.window, 'Save File', os.getenv('HOME'), filter='*.cfl'
        )[0]

        print("save from cfl1=", self.cfl_file_name,"\n")
        self.write_out_cfl()

    def write_out_cfl(self):
        save_atm = []
        print("range of table=",range(self.window.tableWidget_ini.rowCount()),"\n")
        for d in range(self.window.tableWidget_ini.rowCount()):
            print("print d= ", d)
            atm = AtomData()
            atm.Zn = str(self.window.tableWidget_ini.item(d, 0).text())
            atm.Lbl = str(self.window.tableWidget_ini.item(d, 1).text())
            atm.x = float(self.window.tableWidget_ini.item(d,2).text())
            atm.y = float(self.window.tableWidget_ini.item(d,3).text())
            atm.z = float(self.window.tableWidget_ini.item(d,4).text())
            atm.b = float(self.window.tableWidget_ini.item(d,5).text())

            save_atm.append(atm)
        print("data save_atom",save_atm)

        data = CflData()
        data.spgr = self.window.spgr_edit.text()
        data.a = self.window.a_edit.text()
        data.b = self.window.b_edit.text()
        data.c = self.window.c_edit.text()
        data.alpha = self.window.alpha_edit.text()
        data.beta = self.window.beta_edit.text()
        data.gamma = self.window.gamma_edit.text()
        data.h = self.window.h_edit.text()
        data.k = self.window.k_edit.text()
        data.l = self.window.l_edit.text()


        crystal = "Title  "
        crystal += "placeholder title" + " \n"
        crystal += "!" + "        a" + "        b" + "        c" + \
                "     alpha" + "    beta" + "   gamma" + "\n"
        crystal += "Cell    " + str(data.a) + "     " + str(data.b) + "     " +\
                str(data.c) + "  " + str(data.alpha) + "  " + str(data.beta) + \
                "  " + str(data.gamma) + "\n"
        crystal += "!     " + "Space Group" + "\n"
        crystal += "Spgr  " + str(data.spgr) + "\n"
        crystal += "SIZE_LG " + "1.05" + "\n"
        crystal += "IPF_RES " + "1440" + "\n"
        crystal += "HKL_PREF " + str(data.h) + " " + str(data.k) + " " + str(data.l) + "\n"
        crystal += "HKL_PREF_WH " + "6 " + "\n"
        crystal += "AZM_IPF " + "0.5" + "\n"
        crystal += "MASK_MIN " + "0.5" + "\n"
        crystal += "!" + "\n"

        cfl_data = str(crystal)

        self.info2 = open(self.cfl_file_name, 'w')
        for c in save_atm:
            print("data of table=",c.Zn,c.Lbl,c.x,c.y,c.z,c.b,"\n")
            var = "Atom "
            var += str(c.Zn) + "  "
            var += str(c.Lbl) + "  "
            var += str(c.x) + "  "
            var += str(c.y) + "  "
            var += str(c.z) + "  "
            var += str(c.b) + " \n"
            cfl_data += str(var)

        self.info2.write(cfl_data)
        self.info2.close()

    def write_new_params(self):
        dt = InstrumentData()
        #dt.x_res = self.window.x_beam_edit.text()
        #dt.y_res = self.window.y_beam_edit.text()
        #dt.x_beam = self.window.x_max_edit.text()
        #dt.y_beam = self.window.y_max_edit.text()
        dt.lambd = self.window.lambda_edit.text()
        #dt.dst_det = self.window.sample_detector_distance_edit.text()
        #dt.diam_det = self.window.detector_diameter_edit.text()

        crystal = "xres" + "\n" + str('2300') + " \n"
        crystal += "yres"  + "\n" + str('2300') + "\n"
        crystal += "xbeam" + "\n" + str('1150') + "\n"
        crystal += "ybeam" + "\n" + str('1150') + "\n"
        crystal += "lambda" + "\n" + str(dt.lambd) + "\n"
        crystal += "dst_det" + "\n" + str('125') + "\n"
        ##### Temporal
        crystal += "UPPER_HALF" + "\n"
        ##### Temporal
        crystal += "diam_det" + "\n" +str('345.0') + "\n"
        ##### Temporal
        crystal += "thet_min" + "\n"+ "0" + "\n"
        ##### Temporal
        crystal += "end"

        dat_data = str(crystal)
        self.info2 = open(self.info_dat, 'w')
        self.info2.write(dat_data)
        self.info2.close()
    
    def readbb(self):
        self.braggb = QFileDialog.getOpenFileName(
            self.window, 'Open Bragg-Brentano File', ".", "Files (*.ASC)")[0]
        print("info from bragg brentano", self.braggb)
        self.read_difractogram_file_bb()

    def readgi(self):
        self.grazingi = QFileDialog.getOpenFileName(
            self.window, 'Open Grazing incidence File', ".", "Files (*.ASC)")[0]
        print("info from grazing incidence", self.grazingi)
        self.read_difractogram_file_gi()



    def read_difractogram_file_bb(self):

        array_txt = np.loadtxt(self.braggb,usecols=(0, 1), skiprows=1)

        x = array_txt[:,0]
        print("degrees=",x)

        y = array_txt[:,1]
        print("intensity=",y)

        data = np.column_stack([x, y])
        np.savetxt('bragg.txt', data)

        return x,y


    def read_difractogram_file_gi(self):

        array_txt = np.loadtxt(self.grazingi,usecols=(0, 1), skiprows=1)

        x = array_txt[:,0]
        print("degrees=",x)

        y = array_txt[:,1]
        print("intensity=",y)

        data = np.column_stack([x, y])
        np.savetxt('grazing.txt', data)

        return x,y

if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
