'''
Program TRICLIN for crystal geometry
Based on Young and Lytton: Journal of Applied Physics 43, 1408 (1972)
'''
import numpy as np
from numpy import sin,cos,pi,abs, arccos, exp, tan
import math
from Pandas_sf import Arrays
from os import(system)

def Triclinic(a,b,c,alpha,beta,gamma):
    np.set_printoptions(suppress=True,precision = 3)

    intensity,fideg,hkl,cg_dir,texture_direction = Arrays()
    rtg = 180.0/math.pi
    alpha = alpha/rtg
    beta = beta/rtg
    gamma = gamma/rtg
    l33a = 1.0 + 2.0*cos(alpha)*cos(beta)*cos(gamma)
    l33b = cos(alpha)**2 + cos(beta)**2 + cos(gamma)**2
    l33c = l33a - l33b
    L = np.array([[a,b*cos(gamma),c*cos(beta)], \
    [0,b*sin(gamma),c*(cos(alpha)-(cos(beta)*cos(gamma)))/sin(gamma)], \
    [0,0, (c*np.sqrt(l33c))/sin(gamma)]])
    print("\nMatrix L:\n", np.array(L))
    Linv = np.linalg.inv(L)
    print("\nInverse Matrix Linv:\n", Linv)

    #Crystallographic direct basis vectors
    #("t" goes for general "triclinic" possible case)
    at = np.array([1,0,0])
    print("\nVector a in crystallographic: at = ",at)
    ac = L.dot(at)
    print("Vector a in cartesian: ac = ",ac)
    bt = np.array([0,1,0])
    print("Vector b in crystallographic: bt = ",bt)
    bc = L.dot(bt)
    print("Vector b in cartesian: bc = ",bc)
    ct = np.array([0,0,1])
    print("Vector c in crystallographic: ct =",ct)
    cc = L.dot(ct)
    print("Vector c in cartesian: cc = ",cc)

    #Volume of the direct unit cell
    a2a3 = np.cross(bc,cc)
    volu = np.dot(ac,a2a3)
    print("Direct cel volume = ","{:.4f}".format(volu))

    print("\nReciprocal basis vectors, referred to the cartesian basis")
    ar = a2a3 / volu
    print("ar = ", ar)
    br = np.cross(cc,ac) / volu
    print("br = ", br)
    cr = np.cross(ac,bc) / volu
    print("cr = ", cr)

    print("\nTransformation matrix for reciprocal vectors Q:")
    Q = np.array([ar, br, cr]).T
    print(Q)
    
    def poles_of_interest(defined_hkl):
        # HKL list of interest:
        list_hkl = defined_hkl
        #print("cgdir",cg_dir)
        print("\nHKL list, shape:", list_hkl, np.shape(list_hkl))
        Vc_list=[]
        for i in list_hkl:
            Vc_list.append(Q.dot(i))
        Vc_list = np.array(Vc_list)
        print("\nReciprocal vectors list (referred to cartesian coordinates), shape:\n", Vc_list,np.shape(Vc_list))
        modVc_list=[]
        for i in Vc_list:
            modVc_list.append(np.sqrt(i[0]**2 + i[1]**2 + i[2]**2))
        modVc_list = np.array(modVc_list)
        lenVc = len(modVc_list)
        print("\nReciprocal vectors moduli, len:",modVc_list,lenVc)
        #print("\nVc_list[0]/modVc_list[0] =",Vc_list[0]/modVc_list[0])
        print("\nPoles' angular coordinates:")
        print("    [h, k, l]    polar azimuth")
        Vc_polar = np.zeros(lenVc)
        Vc_azim = np.zeros(lenVc)
        for k in range(0, lenVc):
            Vc_polar[k] = math.acos(Vc_list[k,2]/modVc_list[k])
            if Vc_polar[k] == 0: Vc_azim[k] = 0
            else: Vc_azim[k] = math.atan2(Vc_list[k,1] , Vc_list[k,0])
            print(list_hkl[k],"{:.3f}".format(rtg * Vc_polar[k]),"{:.3f}".format(rtg * Vc_azim[k]))
        theta_list = Vc_polar
        phi_list = Vc_azim
        thetadeg_list = Vc_polar*rtg
        phideg_list = Vc_azim*rtg
        print("theta_list", theta_list)
        print("phi_list", phi_list)
        print("thetadeg_list", thetadeg_list)
        print("phideg_list", phideg_list)
        return theta_list,phi_list,thetadeg_list,phideg_list

    ##cg dir
    hkl_test = cg_dir
    angles1 = poles_of_interest(hkl_test)
    theta_list = angles1[0]
    phi_list = angles1[1]
    thetadeg_list = angles1[2]*rtg
    phideg_list = angles1[3]*rtg
    print(f'testing_function {angles1}')
    print(f'theta_list {theta_list}')
    print(f'phi_list {phi_list}')
    print(f'thetadeg_list {thetadeg_list}')
    print(f'phideg_list {phideg_list}')
    
    # HKL list of interest: texture direction
    texture_dir = texture_direction
    mod_list = []
    mod_list.append(texture_dir)
    print(f'mod_list {mod_list}')
    angles2 = poles_of_interest(mod_list)
    thetadeg = angles2[2]
    phideg = angles2[3]
    print(f'test_2crystal {angles2}')
    print(f'thetadeg {thetadeg}')
    print(f'phideg {phideg}')
    
    #HKL list of interest:
    hkl_list = hkl
    angles3 = poles_of_interest(hkl_list)
    taudeg = angles3[2]
    gammadeg = angles3[3]
    print(f'taudeg {taudeg}')
    print(f'gammadeg {gammadeg}')

    return (a,b,c,alpha,beta,gamma,cg_dir,texture_direction,thetadeg,phideg,taudeg,gammadeg,theta_list,thetadeg_list, \
        phi_list,phideg_list)



if __name__ == "__main__":
    #thetadeg,phideg,taudeg,gammadeg = Ortorrombic()
   a,b,c,alpha,beta,gamma,cg_dir,texture_direction,thetadeg,phideg,taudeg,gammadeg,theta_list,thetadeg_list, \
        phi_list,phideg_list = Triclinic()