Grazing 

Module for 1D Texture modelling

Getting started:
1.Install miniconda from:

https://docs.conda.io/en/latest/miniconda.html   *** be sure to install miniconda on your username folder C:User/username

2.open Anaconda prompt and type: 

curl -o Grazing_dependencies.bat https://gitlab.com/cimav-tools/grazing/-/raw/main/Src/Grazing_dependencies.bat --ssl-no-revoke

3. A Batch named:"Grazing dependencies.bat" will be donwloaded,run the file in the path you want the repository to be on.
4. Run batch named "compile_exes.bat on src folder
5. In order to open grazing run "open_grazin.bat" file

Note: Space groups need to be edited manually, they are listed as following:

 C1: 1;   Ci: 2;   C2: 3;   Cs: 4;  C2h: 5;   D2: 6;  C2v: 7; D2h: 8"
 C4: 9;   S4: 10; C4h: 11;  D4: 12; C4v: 13; D2d: 14; D4h: 15; C3: 16"
 S6: 17;  D3: 18; C3v: 19; D3d: 20;  C6: 21; C3h: 22; C6h: 23; D6: 24"
 C6v: 25; D3h: 26; D6h: 27;   T: 28;  Th: 29;   O: 30;  Td: 31; Oh: 32"